<?php

namespace Babar\Product;

use Illuminate\Support\ServiceProvider;

class ProductServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

        // register our controller
        $this->app->make('Babar\Product\ProductController');
        $this->loadViewsFrom(__DIR__.'/views', 'product');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        include __DIR__.'/routes.php';
        $this->loadMigrationsFrom(__DIR__.'/2022_01_25_093458_create_products_table.php');
    }
}
