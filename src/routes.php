<?php

Route::get('product', function(){
	echo 'Hello from the product package!';
});

Route::get('add/{a}/{b}', 'Babar\Product\ProductController@add');
Route::get('subtract/{a}/{b}', 'Babar\Product\ProductController@subtract');
Route::get('test', 'Babar\Product\ProductController@test');


// Route::group([

//     'prefix' => 'api'

// ], function ($router) {
//     Route::post('/login', 'AuthController@login');
//     Route::post('/logout', 'AuthController@logout');
//     Route::post('/refresh', 'AuthController@refresh');
//     Route::get('/user-profile', 'AuthController@me');


    Route::get('/api/product' , 'Babar\Product\ProductController@index');
    Route::get('/api/product/{id}' , 'Babar\Product\ProductController@show');
    Route::post('/api/create-product' , 'Babar\Product\ProductController@create');
    Route::put('/api/update-product/{id}' , 'Babar\Product\ProductController@update');
    Route::delete('/api/delete-product/{id}' , 'Babar\Product\ProductController@delete');

// });

