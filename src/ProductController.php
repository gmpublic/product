<?php

namespace Babar\Product;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Babar\Product\Product;

class ProductController extends Controller
{
    //
    // public function __construct()
    // {
    //     $this->middleware('auth:api', ['except' => ['login', 'refresh', 'logout']]);
    // }


    public function index(){
        $Product = Product::all();
        return response()->json($Product);
    }


    public function show($id){

        $Product = Product::find($id);
        return response()->json($Product);

    }

    public function create(Request $request){


        $Product = new Product();
        $Product->name = $request->name;
        $Product->price = $request->price;
        $Product->description = $request->description;
        $Product->save();

        return response()->json([
            "message" => "Product Added Successfully",
            "product" => $Product
        ]);



    }

    public function update(Request $request , $id){


        $product = Product::find($id);

        $product->name = !empty($request->name) ? $request->name : $product->name;
        $product->price = !empty($request->price) ? $request->price : $product->price;
        $product->description = !empty($request->description) ? $request->description : $product->description;
        $product->save();

        return response()->json([
            "message" => "Product Updated Successfully",
             "product" => $product
        ]);

    }

    public function delete($id){

        $Product = Product::find($id);
        $Product->delete();
        return response()->json("Product deleted Successfull");

    }
}
