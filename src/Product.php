<?php

// namespace App\Models;
namespace Babar\Product;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    // protected $table = 'tbl_countries';
    protected $guarded = [];
    protected $fillable = [
        'name',
        'price',
        'description'
    ];
}
